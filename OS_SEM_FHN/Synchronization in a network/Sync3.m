%Synchronization in networks of two FHN neurons
clear;
clc;
ne=25;
T=200;
Nt=100000;
dt=T/Nt;
RKstep=1;
theta=0.5;
x1=0;x2=100;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:ne
np(i)=6;
end
xe=linspace(x1,x2,ne+1);
for l=1:ne

  m = np(l);
 [xi,wi] = lglnodes(m);
  mx = 0.5*(xe(l+1)+xe(l));
  dx = 0.5*(xe(l+1)-xe(l));

  for j=1:m+1
    xen(l,j) = mx + xi(m-j+2)*dx;
  end

  for j=1:m+1
    xien(l,j) =  xi(m-j+2);
  end

end
Ic = 2;      % global node counter
for l=1:ne
  Ic = Ic-1;
  for j=1:np(l)+1
    c(l,j) = Ic;
    xg(Ic) = xen(l,j);
    Ic = Ic+1;
  end
end

ng = Ic-1;
%%%%%%%%%
[X,Y]=meshgrid(xg,xg);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%initial conditions
v2    = 1*ones(ng).*(X<=50).*(Y>=50)-1*ones(ng).*(X>=50).*(Y<=50);
u2    = 1*ones(ng).*(X>=50).*(Y>=50)-1*ones(ng).*(X<=50).*(Y<=50);
u1=ones(ng);
v1=u1;
v3=v2;
u3=u2;
U2(:,:)=u2;
V2(:,:)=v2;
U1(:,:)=u1;
V1(:,:)=v1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

zr=zeros(ng,1);
[M,K]=gm_lob(ne,xe,np,ng,c);
L=M+theta*dt*10*K;
Pu=inv(L);
L=M+theta*dt*K;
Pv=inv(L);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
U3(:,:)=u3;
V3(:,:)=v3;
U2(:,:)=u2;
V2(:,:)=v2;
U1(:,:)=u1;
V1(:,:)=v1;
%%%%%%%%%%%%%%% Parameters%%%%%%%%%%%%%%%
h=dt/RKstep;
eps=0.1;
delta=0.001;
beta=0.252;
%%%%%%%%%%%%%%% define RK function%%%%%%%
f1=@(yi,zi) 1/eps.*(-yi.^3+3*yi-zi);
f2=@(yi,zi) yi-delta.*zi;
f3=@(yi,zi,vv) yi-delta.*zi+beta*(vv-zi);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    TT=zeros(Nt+1,1);
    ERROR=zeros(Nt+1,1);
tic;
for j=1:Nt
    TT(j)=j*dt;
	%%%%%%%%%%%%%%%Diffusion u on x direction  for neuron 1%%%%%%%%%%%%%%%%
    R=(M-((1-theta)*dt*10)*K)*U1';
    U1=(Pu*R)';
    %%%%%%%%%%%%%%%Diffusion u on y direction  for neuron 1%%%%%%%%%%%%%%%%
    R=(M-((1-theta)*dt*10)*K)*U1;
    U1=(Pu*R);
	%%%%%%%%%%%%%%%Diffusion v in x direction  for neuron 1%%%%%%%%%%%%%%%%
	R=(M-((1-theta)*dt)*K)*V1';
    V1=(Pv*R)';
	%%%%%%%%%%%%%%%Diffusion v in y direction  for neuron 1%%%%%%%%%%%%%%%%
	R=(M-((1-theta)*dt)*K)*V1;
    V1=(Pv*R);
	%%%%%%%%%%%%%%%reaction of u and v for neuron 1%%%%%%%%%%%%%%%%%
	y=zeros(ng,ng,RKstep+1);
    z=zeros(ng,ng,RKstep+1);
    y(:,:,1)=U1;
    z(:,:,1)=V1;
    for i=1:RKstep
        k0=h.*f1(y(:,:,i),z(:,:,i));
        l0=h.*f2(y(:,:,i),z(:,:,i));
        k1=h.*f1(y(:,:,i)+1/2.*k0,z(:,:,i)+1/2.*l0);
        l1=h.*f2(y(:,:,i)+1/2.*k0,z(:,:,i)+1/2.*l0);
        k2=h.*f1(y(:,:,i)+1/2.*k1,z(:,:,i)+1/2.*l1);
        l2=h.*f2(y(:,:,i)+1/2.*k1,z(:,:,i)+1/2.*l1);
        k3=h.*f1(y(:,:,i)+k2,z(:,:,i)+l2);
        l3=h.*f2(y(:,:,i)+k2,z(:,:,i)+l2);
        y(:,:,i+1)=y(:,:,i)+1/6.*(k0+2.*k1+2.*k2+k3);
        z(:,:,i+1)=z(:,:,i)+1/6.*(l0+2.*l1+2.*l2+l3);
    end
	U1=y(:,:,i+1);
	V1=z(:,:,i+1);
    %%%%%%%%%%%%%%%Diffusion u on x direction  for neuron 2%%%%%%%%%%%%%%%%
	R=(M-((1-theta)*dt*10)*K)*U2';
    U2=(Pu*R)';
	%%%%%%%%%%%%%%%Diffusion u on y direction  for neuron 2%%%%%%%%%%%%%%%%
    R=(M-((1-theta)*dt*10)*K)*U2;
    U2=(Pu*R);
	%%%%%%%%%%%%%%%Diffusion v on x direction  for neuron 2%%%%%%%%%%%%%%%%
    R=(M-((1-theta)*dt)*K)*V2';
    V2=(Pv*R)';
	%%%%%%%%%%%%%%%Diffusion of v on y direction  for neuron 2%%%%%%%%%%%%%
	R=(M-((1-theta)*dt)*K)*V2;
    V2=(Pv*R);
	%%%%%%%%%%%%%%%reaction of u and v for neuron 2%%%%%%%%%%%%%%%%%
	y(:,:,1)=U2;
    z(:,:,1)=V2;
    for i=1:RKstep
        k0=h.*f1(y(:,:,i),z(:,:,i));
        l0=h.*f3(y(:,:,i),z(:,:,i),V1);
        k1=h.*f1(y(:,:,i)+1/2.*k0,z(:,:,i)+1/2.*l0);
        l1=h.*f3(y(:,:,i)+1/2.*k0,z(:,:,i)+1/2.*l0,V1);
        k2=h.*f1(y(:,:,i)+1/2.*k1,z(:,:,i)+1/2.*l1);
        l2=h.*f3(y(:,:,i)+1/2.*k1,z(:,:,i)+1/2.*l1,V1);
        k3=h.*f1(y(:,:,i)+k2,z(:,:,i)+l2);
        l3=h.*f3(y(:,:,i)+k2,z(:,:,i)+l2,V1);
        y(:,:,i+1)=y(:,:,i)+1/6.*(k0+2.*k1+2.*k2+k3);
        z(:,:,i+1)=z(:,:,i)+1/6.*(l0+2.*l1+2.*l2+l3);
    end
	U2=y(:,:,i+1);
	V2=z(:,:,i+1);
	%%%%%%%%%%%%%%%Diffusion u on x direction  for neuron 3%%%%%%%%%%%%%%%%
	R=(M-((1-theta)*dt)*K)*U3';
    U3=(Pu*R)';
	%%%%%%%%%%%%%%%Diffusion u on y direction  for neuron 3%%%%%%%%%%%%%%%%
	R=(M-((1-theta)*dt)*K)*U3;
    U3=(Pu*R);
	%%%%%%%%%%%%%%%Diffusion v on x direction  for neuron 3%%%%%%%%%%%%%%%%
	R=(M-((1-theta)*10*dt)*K)*V3';
    V3=(Pv*R)';
	%%%%%%%%%%%%%%%Diffusion of v on y direction  for neuron 3%%%%%%%%%%%%%
	R=(M-((1-theta)*10*dt)*K)*V3;
    V3=(Pv*R);
	%%%%%%%%%%%%%%%reaction of u and v for neuron 3%%%%%%%%%%%%%%%%%
	y(:,:,1)=U3;
    z(:,:,1)=V3;
    for i=1:RKstep
        k0=h.*f1(y(:,:,i),z(:,:,i));
        l0=h.*f3(y(:,:,i),z(:,:,i),V2);
        k1=h.*f1(y(:,:,i)+1/2.*k0,z(:,:,i)+1/2.*l0);
        l1=h.*f3(y(:,:,i)+1/2.*k0,z(:,:,i)+1/2.*l0,V2);
        k2=h.*f1(y(:,:,i)+1/2.*k1,z(:,:,i)+1/2.*l1);
        l2=h.*f3(y(:,:,i)+1/2.*k1,z(:,:,i)+1/2.*l1,V2);
        k3=h.*f1(y(:,:,i)+k2,z(:,:,i)+l2);
        l3=h.*f3(y(:,:,i)+k2,z(:,:,i)+l2,V2);
        y(:,:,i+1)=y(:,:,i)+1/6.*(k0+2.*k1+2.*k2+k3);
        z(:,:,i+1)=z(:,:,i)+1/6.*(l0+2.*l1+2.*l2+l3);
    end
	U3=y(:,:,i+1);
	V3=z(:,:,i+1); 
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	ERROR(j)=sqrt(norm(U2(:,:)-U3(:,:))^2+norm(V2(:,:)-V3(:,:))^2+norm(U1(:,:)-U2(:,:))^2+norm(V1(:,:)-V2(:,:))^2);
end
toc;