function mm = emm_lob (m,k)
[x,wii]=lglnodes(m);
[zi,wi]=lglnodes(k);
for j=1:m+1
    xi(j) = x(m-j+2);
end
for j=1:k+1
    z(j) = zi(k-j+2);
    w(j) = wi(k-j+2);
end

%--------------------------
% compute the mass matrix
% by the Lobatto quadrature
%--------------------------

for ind=1:m+1 
   for jnd=1:m+1       


      for j=1:k+1

       f(j) = 1.0;

       for i=1:m+1
        if(i ~= ind)
         f(j) = f(j)*(z(j)-xi(i))/(xi(ind)-xi(i));
        end
        if(i ~= jnd)
         f(j) = f(j)*(z(j)-xi(i))/(xi(jnd)-xi(i));
        end
       end

      end  % of loop over j

  % perform the quadrature

     msmat = 0.0;

     for i=1:k+1
       msmat = msmat + f(i)*w(i);
     end

    mm(ind,jnd) = msmat;

   end
end

return;
