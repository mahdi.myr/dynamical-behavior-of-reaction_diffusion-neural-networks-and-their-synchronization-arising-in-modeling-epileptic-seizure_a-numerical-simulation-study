clear; 
clc;
T=5;
Nt=50000;
ratio=5.0;
theta=0.5;
dt=T/Nt;
D=1;
a=0.1;
b=0.5;
Av=1;
alpha=1;
eps=0.001;
Gamma=0.5;
ne=53;
RKstep=1;
x1=-20;x2=20;
%%%%%%
for i=1:ne
np(i)=15;
end
xe=linspace(x1,x2,ne+1);
for l=1:ne

  m = np(l);
 [xi,wi] = lglnodes(m);
  mx = 0.5*(xe(l+1)+xe(l));
  dx = 0.5*(xe(l+1)-xe(l));

  for j=1:m+1
    xen(l,j) = mx + xi(m-j+2)*dx;
  end

  for j=1:m+1
    xien(l,j) =  xi(m-j+2);
  end

end
Ic = 2;      % global node counter
for l=1:ne
  Ic = Ic-1;
  for j=1:np(l)+1
    c(l,j) = Ic;
    xg(Ic) = xen(l,j);
    Ic = Ic+1;
  end
end

ng = Ic-1;
%%%%%%%%%
%%%initial conditions
[X,Y]=meshgrid(xg,xg);
u0    = ((1+exp(4.*(abs(X)-3))).^-2-(1+exp(4.*(abs(X)-1))).^-2).*((X<0)+(Y>5)<1);
v0    = 0.15.*(X<1).*(Y<-10);
u(:,:)=u0;
v(:,:)=v0;
%%%%%%%%%
zr=zeros(ng,1);
[M,K]=gm_lob(ne,xe,np,ng,c);
tic;
L=M+theta*dt*K; %Global mass and diffusion matrices
L_inv=inv(L);

for i=1:Nt
    %Deffusion sub-problem on x
    R=(M-((1-theta)*dt)*K)*u';
    u=(L_inv*R)';
    
    %Deffusion sub-problem on y
 	R=(M-((1-theta)*dt)*K)*u;
    u=(L_inv*R);

    %reaction
    h=dt/RKstep;
    y=zeros(size(u,1),size(u,2),RKstep+1);
    z=zeros(size(v,1),size(v,2),RKstep+1);
    y(:,:,1)=u;
    z(:,:,1)=v;
    for j=1:RKstep
        yi=y(:,:,j);
        zi=z(:,:,j);
        k0=h*( 1/eps.*(Av*yi.*(1-yi).*(yi-a)-zi));
        l0=h*(alpha.*yi-Gamma.*zi);
    
        yi=y(:,:,j)+1/2.*k0;
        zi=z(:,:,j)+1/2.*l0;
        k1=h*( 1/eps.*(Av*yi.*(1-yi).*(yi-a)-zi));
        l1=h*(alpha.*yi-Gamma.*zi);
    
        yi=y(:,:,j)+1/2.*k1;
        zi=z(:,:,j)+1/2.*l1;
        k2=h*( 1/eps.*(Av*yi.*(1-yi).*(yi-a)-zi));
        l2=h*(alpha.*yi-Gamma.*zi);
    
        yi=y(:,:,j)+k2;
        zi=z(:,:,j)+l2;
        k3=h*(1/eps.*(Av*yi.*(1-yi).*(yi-a)-zi));
        l3=h*(alpha.*yi-Gamma.*zi);
    
        y(:,:,j+1)=y(:,:,j)+1/6.*(k0+2.*k1+2.*k2+k3);
        z(:,:,j+1)=z(:,:,j)+1/6.*(l0+2.*l1+2.*l2+l3);
    end
    u=y(:,:,j+1);
    v=z(:,:,j+1);
  
end
toc;