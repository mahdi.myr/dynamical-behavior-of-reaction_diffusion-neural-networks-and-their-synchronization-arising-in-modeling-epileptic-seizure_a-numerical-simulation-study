function dm = edmm_lob (m)
[x,wi]=lglnodes(m);
for j=1:m+1
    xi(j) = x(m-j+2);
    w(j) = wi(m-j+2);
end

%--------------------------
% compute the mass matrix
% by the Lobatto quadrature
%--------------------------

for i=1:m+1
  for j=1:m+1

      if(i ~= j)
        ndm(i,j) = 1.0/(xi(j)-xi(i));
        for l=1:m+1
          if(l ~= j) ndm(i,j) = ndm(i,j)*(xi(j)-xi(l)); end
          if(l ~= i) ndm(i,j) = ndm(i,j)/(xi(i)-xi(l)); end
        end
      
      else
         ndm(i,i) = 0.0;
         for l=1:m+1
           if(i ~= l) ndm(i,i) = ndm(i,i) +1.0/(xi(i)-xi(l)); end
         end
      end
  end
end

%----------------------------
% compute the difusion matrix
% by the Lobatto quadrature
%----------------------------

for i=1:m+1    
   for j=1:m+1       

     dm(i,j) = 0.0;

     for l=1:m+1
       dm(i,j) = dm(i,j) + ndm(i,l)*ndm(j,l)*w(l);
     end

   end
end

return;
