clear;
clc;
T=5;
Nt=10000;
dt=T/Nt;
eps=0.005;
theta=0.5;
a=0.3;
b=0.01;
D=1;
RKstep=20;
ratio=5.0;
ne=100;
x1=-30; x2=30;
%%%%%%%%%%%%%%%%%%
for i=1:ne
np(i)=12;
end
xe=linspace(x1,x2,ne+1);
for l=1:ne

  m = np(l);
 [xi,wi] = lglnodes(m);%GLL nodes and weights
  mx = 0.5*(xe(l+1)+xe(l));
  dx = 0.5*(xe(l+1)-xe(l));

  for j=1:m+1
    xen(l,j) = mx + xi(m-j+2)*dx;
  end

  for j=1:m+1
    xien(l,j) =  xi(m-j+2);
  end

end
Ic = 2;      % global node counter
for l=1:ne
  Ic = Ic-1;
  for j=1:np(l)+1
    c(l,j) = Ic;
    xg(Ic) = xen(l,j);
    Ic = Ic+1;
  end
end

ng = Ic-1;
%%%%%%%%%
%%%initial conditions
u0    = ((1+exp(4.*(abs(xg)-25))).^-2-(1+exp(4.*(abs(xg)-21))).^-2).*(xg<0);
v0    = 0.3.*(xg<-25);
u(:,1)=u0;
v(:,1)=v0;
%%%%%%%%%%%%
zr=zeros(ng,1);
[M,K]=gm_lob(ne,xe,np,ng,c);%Global mass and diffusion matrices
tic;
L=M+theta*dt*K;
L_inv=inv(L);
for i=1:Nt
%Method: Trotter operator-splitting method [D_x D_y R]^n
    u1=heat2(u(:,i),L_inv,M,K,dt,2);%Diffusion subproblem
    [u(:,i+1) v(:,i+1)]=RK4SYS1(u1,v(:,i),dt,RKstep);%Reaction subproblem
end
toc