function [U,V]=RK4SYS1(u,v,T,step)
h=T/step;
%FHN Paremeters
eps=0.005;
a=0.3;
b=0.01;
alpha=1;
Gamma=1;
%%%%%%%%%%
y=zeros(size(u,1),size(u,2),step+1);
z=zeros(size(v,1),size(v,2),step+1);
y(:,1)=u;
z(:,1)=v;
f1=@(xi,yi,zi) 1/eps.*(yi.*(1-yi).*(yi-(zi+b)./a));
f2=@(xi,yi,zi) alpha.*yi-Gamma.*zi;
for i=1:step
k0=h.*f1(0,y(:,i),z(:,i));
l0=h.*f2(0,y(:,i),z(:,i));
k1=h.*f1(0+1/2*h,y(:,i)+1/2.*k0,z(:,i)+1/2.*l0);
l1=h.*f2(0+1/2*h,y(:,i)+1/2.*k0,z(:,i)+1/2.*l0);
k2=h.*f1(0+1/2*h,y(:,i)+1/2.*k1,z(:,i)+1/2.*l1);
l2=h.*f2(0+1/2*h,y(:,i)+1/2.*k1,z(:,i)+1/2.*l1);
k3=h.*f1(0+h,y(:,i)+k2,z(:,i)+l2);
l3=h.*f2(0+h,y(:,i)+k2,z(:,i)+l2);
y(:,i+1)=y(:,i)+1/6.*(k0+2.*k1+2.*k2+k3);
z(:,i+1)=z(:,i)+1/6.*(l0+2.*l1+2.*l2+l3);
end
U=y(:,i+1);
V=z(:,i+1);
return
