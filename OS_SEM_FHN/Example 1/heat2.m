function u=heat2(u,L_inv,M,K,dt,dir)
theta=0.5;
if dir<2 %x-direction
    trans=1;
    w=u';
else %y-direction
    w=u;
    trans=0;
end
R=(M-((1-theta)*dt)*K)*w;
u=L_inv*R;
if trans>0
    u=u';
    
end
end


